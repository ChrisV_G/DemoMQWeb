package test.message.demo.controller;


import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.amqp.rabbit.annotation.RabbitListenerConfigurer;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.listener.RabbitListenerEndpointRegistrar;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.amqp.rabbit.listener.adapter.MessageListenerAdapter;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.PropertySource;
import org.springframework.messaging.converter.MappingJackson2MessageConverter;
import org.springframework.messaging.handler.annotation.support.DefaultMessageHandlerMethodFactory;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.core.env.Environment;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import test.message.demo.services.CustomMessageListener;

@SpringBootApplication
@EnableWebMvc
@EnableScheduling
@EnableRabbit
@ComponentScan(basePackages = "test.message.demo")
@PropertySource(value = { "classpath:application.properties" })
public class DemoApplication  {


	@Autowired
	private Environment env;



	@Bean
	Queue queue() {
		return new Queue(env.getRequiredProperty("messagequeue.routingKey"), false);
	}

	@Bean
	TopicExchange exchange() {
		return new TopicExchange(env.getRequiredProperty("messagequeue.exchangeName"));
	}

	@Bean
	Binding binding(Queue queue, TopicExchange exchange) {
		return BindingBuilder.bind(queue).to(exchange).with("foo.bar.#");
	}

	@Bean
	SimpleMessageListenerContainer container(ConnectionFactory connectionFactory
            ,MessageListenerAdapter listenerAdapter
	) {
		SimpleMessageListenerContainer container = new SimpleMessageListenerContainer();
		container.setConnectionFactory(connectionFactory);
		container.setQueueNames(env.getRequiredProperty("messagequeue.routingKey"));
        container.setMessageListener(listenerAdapter);
		return container;
	}

    @Bean
	MessageListenerAdapter listenerAdapter(CustomMessageListener receiver) {
        return new MessageListenerAdapter(receiver, "receiveMessage");
    }

//	@Bean
//	public ConnectionFactory connectionFactory() {
//		return new CachingConnectionFactory("192.168.99.100");
//	}

		@Bean
	public ConnectionFactory connectionFactory() {
		CachingConnectionFactory factory = new CachingConnectionFactory();
		factory.setUsername(env.getRequiredProperty("messagequeue.userName"));
		factory.setPassword(env.getRequiredProperty("messagequeue.password"));
		factory.setHost(env.getRequiredProperty("messagequeue.hostName"));
		factory.setPort(Integer.parseInt(env.getRequiredProperty("messagequeue.portNumber")));
		return factory;
	}

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}

	}

