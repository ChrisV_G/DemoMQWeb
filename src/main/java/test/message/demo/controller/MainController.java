package test.message.demo.controller;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import test.message.demo.model.CustomMessage;
import test.message.demo.services.CustomMessageListener;
import test.message.demo.services.CustomMessageSender;

@RestController
public class MainController {

    @Autowired
    private CustomMessageListener customMessageReceiver;

    @Autowired
    private CustomMessageSender customMessageSender;

//    @RequestMapping("/getMessage")
//    public ResponseEntity<?> getAllProcesses() {
//        try {
//            return ResponseEntity.ok(customMessageReceiver.receiveMessage());
//        } catch (Exception e) {
//            return ResponseEntity.badRequest().build();
//        }
//    }

    @RequestMapping(value = "/sendMessage", method = RequestMethod.POST)
    public ResponseEntity<?> skippingServicesTask(@RequestBody CustomMessage process) {
        try {
            customMessageSender.sendMessage(process);
            return ResponseEntity.ok("ok");
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.badRequest().build();
        }
    }

}


